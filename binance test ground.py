
from os import write
from binance import Client
from binance.enums import *
from datetime import datetime

import json
import time
import math
client = Client("DlvEppYNE7GidRajk4f8Zl75p1H5qNyRCgygW1nEPlb7KmQRbVGSEiaMjCYSCU2J", "pw8HxZhHKmIMNXfInR78bmLUL97iqVmmg5ZCeoQgwe7D1XwMSB8W6N9x5OU4XhXe")
print(client.get_system_status())


def getValueInDict(key,dictt):
    if key in dictt:
        return dictt[key]
    else:
        return None

def writeLog(text):
    with open("testground.txt", "a") as myfile:
        dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        myfile.write(dt_string + ": " +text + "\n")

def conditionsSastified(symbol,datetimemillis,priceThreshold):
    candles5m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, datetimemillis - 5*60*60*1000,datetimemillis)
    candles5m = list(reversed(candles5m))

    if len(candles5m) < 3:
        writeLog("NO_rANGE_AVOID " + symbol)
        permanentExcludedPairs[symbol] = {"price" : 0, "time": (time.time() + 60*20)}
        return None

    candles5m_vol_0 = float(candles5m[0][7]) if float(candles5m[0][7]) !=0 else 1
    candles5m_vol_1 = float(candles5m[1][7]) if float(candles5m[1][7]) !=0 else 1
    candles5m_vol_2 = float(candles5m[2][7]) if float(candles5m[2][7]) !=0 else 1
    candles5m_vol_3 = float(candles5m[3][7]) if float(candles5m[3][7]) !=0 else 1
    candles5m_vol_4 = float(candles5m[4][7]) if float(candles5m[4][7]) !=0 else 1
    candles5m_vol_5 = float(candles5m[5][7]) if float(candles5m[5][7]) !=0 else 1
    candles5m_change_0 = float(candles5m[0][4])/float(candles5m[0][1]) - 0.9999
    candles5m_change_1 = float(candles5m[1][4])/float(candles5m[1][1]) - 0.9999
    candles5m_change_2 = float(candles5m[2][4])/float(candles5m[2][1]) - 0.9999
    candles5m_change_3 = float(candles5m[3][4])/float(candles5m[3][1]) - 0.9999
    candles5m_change_4 = float(candles5m[4][4])/float(candles5m[4][1]) - 0.9999
    candles5m_v_c_0 = float(candles5m[0][4])
    candles5m_v_o_0 = float(candles5m[0][1])

    candles5m_consecutive_green = 0
    candles5m_consecutive_green_amp = 0
    candles5m_amp_last_30m = 0
    candles5m_high_amp_last_30m = 0
    candles5m_consecutive_green_boolean = True
    candles5m_consecutive_green_low_amp = True

    # {"type" : "tip/dip", "v_c" : 0.3, "candle_index" :0}
    dip_array =[{"v_c" : candles5m_v_c_0, "candle_index" :0}]
    tip_array =[{"v_c" : candles5m_v_c_0, "candle_index" :0}]

    lowest_from_last_tip_index = 0
    lowest_from_last_tip_v_c = candles5m_v_c_0
    highest_from_last_dip_index = 0
    highest_from_last_dip_v_c = candles5m_v_c_0
    for index,candle in enumerate(candles5m):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        v_change = abs(v_c/v_o - 0.9999)

        if index <= 8 and index > 1 and v_h/v_l > 1.02:
            candles5m_high_amp_last_30m = candles5m_high_amp_last_30m + 1
        if candles5m_consecutive_green_boolean is True:
            if v_c/v_o >= 0.998:
                candles5m_consecutive_green = candles5m_consecutive_green + 1
                if v_c > v_o:
                    candles5m_consecutive_green_amp = candles5m_consecutive_green_amp + v_change
                else: 
                    candles5m_consecutive_green_amp = candles5m_consecutive_green_amp - v_change
                if index == 1:
                    if v_change > 0.02:
                        candles5m_consecutive_green_low_amp = False
                if index > 1:
                    if v_change/candles5m_change_0 > 0.4: 
                        candles5m_consecutive_green_low_amp = False
            else:
                candles5m_consecutive_green_boolean = False
        if index <= 8:
            if v_c >= v_o:
                candles5m_amp_last_30m = candles5m_amp_last_30m + v_change
            else:
                candles5m_amp_last_30m = candles5m_amp_last_30m - v_change
        last_tip = tip_array[-1]
        last_tip_v_c = last_tip["v_c"]
        last_tip_candle_index = last_tip["candle_index"]
        second_last_tip = tip_array[-2] if len(tip_array) >= 2 else tip_array[-1]
        second_last_tip_v_c = second_last_tip["v_c"]
        second_last_tip_candle_index = second_last_tip["candle_index"]
        last_dip = dip_array[-1]
        last_dip_v_c = last_dip["v_c"]
        last_dip_candle_index = last_dip["candle_index"]
        second_last_dip = dip_array[-2] if len(dip_array) >= 2 else dip_array[-1]
        second_last_dip_v_c = second_last_dip["v_c"]
        second_last_dip_candle_index = second_last_dip["candle_index"]

        selected_tip_v = v_c if v_c > v_o else v_o
        if index - 3 > last_tip_candle_index:
            if last_dip_candle_index == 0:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
            #elif second_last_dip_candle_index > second_last_tip_candle_index and selected_tip_v > lowest_from_last_tip_v_c:
            elif selected_tip_v/lowest_from_last_tip_v_c >= 1.007:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
        else:
            if selected_tip_v > last_tip_v_c:
                if lowest_from_last_tip_index > last_tip_candle_index and selected_tip_v/lowest_from_last_tip_v_c > 1.005 and last_tip_v_c/lowest_from_last_tip_v_c > 1.005:
                    pass
                else:
                    tip_array.pop()
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v

        last_tip = tip_array[-1]
        last_tip_v_c = last_tip["v_c"]
        last_tip_candle_index = last_tip["candle_index"]
        second_last_tip = tip_array[-2] if len(tip_array) >= 2 else tip_array[-1]
        second_last_tip_v_c = second_last_tip["v_c"]
        second_last_tip_candle_index = second_last_tip["candle_index"]
        last_dip = dip_array[-1]
        last_dip_v_c = last_dip["v_c"]
        last_dip_candle_index = last_dip["candle_index"]
        second_last_dip = dip_array[-2] if len(dip_array) >= 2 else dip_array[-1]
        second_last_dip_v_c = second_last_dip["v_c"]
        second_last_dip_candle_index = second_last_dip["candle_index"]

        selected_dip_v = v_c if v_c < v_o else v_o
        if index - 3 > last_dip_candle_index:
            if last_tip_candle_index == 0:
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
            #elif second_last_tip_candle_index > second_last_dip_candle_index and selected_dip_v < highest_from_last_dip_v_c:
            elif highest_from_last_dip_v_c/selected_dip_v >= 1.007 :
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
        else:
            if selected_dip_v < last_dip_v_c:
                if highest_from_last_dip_index > last_dip_candle_index and highest_from_last_dip_v_c/selected_dip_v > 1.005 and highest_from_last_dip_v_c/last_dip_v_c > 1.005:
                    pass
                else:
                    dip_array.pop()
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v

        if lowest_from_last_tip_index - 3 <= index:
            if selected_tip_v < lowest_from_last_tip_v_c :
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
        if highest_from_last_dip_index - 3 <= index:
            if selected_dip_v > highest_from_last_dip_v_c:
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v

    for index,dip in enumerate(dip_array):
        writeLog("DIP " + symbol + " " + str(dip))

    for index,tip in enumerate(tip_array):
        writeLog("TIP " + symbol + " " + str(tip))
    return None

open('testground.txt', 'w').close()
dt_obj = datetime.strptime('08.12.2022 10:15:04','%d.%m.%Y %H:%M:%S')
millisec = int(dt_obj.timestamp() * 1000)
print(conditionsSastified("FXS"+"USDT", millisec, 1.014))
# def loopIt():
#     conditionsSastified("SRMUSDT")

# writeLog(" ------- BEGIN -------")
# while True:
#     writeLog("-----------------------------------------------------")
#     loopIt()
#     time.sleep(5) 