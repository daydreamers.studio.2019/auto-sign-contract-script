
from os import write
from binance import Client
from binance.enums import *
from datetime import datetime

import json
import time
import random
import math
import operator
import pathlib
import traceback

#threshold < 1.07 , bỏ rule 0m20s, candle 0 phải > 1.01,usdt5m/usdt5m2h > 20, min_hold_duration =1m, threshold>1.04, bỏ vol<200k
#client = Client("DlvEppYNE7GidRajk4f8Zl75p1H5qNyRCgygW1nEPlb7KmQRbVGSEiaMjCYSCU2J", "pw8HxZhHKmIMNXfInR78bmLUL97iqVmmg5ZCeoQgwe7D1XwMSB8W6N9x5OU4XhXe",{"timeout": 5})
#client = Client("kDXhAuyK4TSK9cdcCjVChak5ujobrR31HQu6xbtS97B2XweSVIjxiJK7c9BK7K8p", "vvGhkrSQj5xuR3rwUrqPx1sA45e8WLROunEpfWljqe3KjF7nbiv728322S6K8sTq",{"timeout": 5})
client = Client("XYwVJdzWucK2yAFfLnarEc5wxjSmiBckW2QPWIkh1SoGD353FTP1sGSzQXFE3caV", "NpUCCfPjLMqeFqllCZUApBqKRPq7pIHxjpgkuXesgZUFPbjy6KEicWHYUY0CXNTX",{"timeout": 5})
log_file_name = "log_batch1"
tab_symbol="&#9;"
print(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + " " + str(client.get_system_status()))

real_trade_mode = False

max_crypto_price_allowed = 500.0
notable_threshold_alert = 1.014
max_threshold_allowed = 1.09
immediate_lower_threshhold_to_sell = 0.974
immediate_upper_threshhold_to_sell = 1.1
max_duration_price_to_sell = 0.997
floor_threshold_to_sell = 1.033
price_falling_threshold_to_sell = 1.05
max_hold_duration = 60 * 60
min_hold_duration = 60 * 0.6
delay_per_turn = 0.2
delay_per_turn_holding = 0.3
excluded_time_out = 60*1
max_coin_hold = 2
btc_down_timed_out = 60 * 10

currency_symbol = "USDT"
currency_ballance = 2000
budget = 1000

totalTrades = 0
totalWonTrades = 0
timed_out = 0

trade_info_short = ["TRADE_INFO_A"]
trade_info_long = ["TRADE_INFO_D","TRADE_INFO_E","TRADE_INFO_F","TRADE_INFO_G","TRADE_INFO_X","TRADE_INFO_Y","TRADE_INFO_FAN_TOKEN","TRADE_INFO_B","TRADE_INFO_C","TRADE_INFO_C1","TRADE_INFO_C2","TRADE_INFO_H","TRADE_INFO_I","TRADE_INFO_K"]
notified_at_minute = -1
global_avoid_time = 0
totalProfit = 0
permanentExcludedPairs = {"NBTUSDT":{},"BTTCUSDT":{}}
excludedPairs = {}
tradingPairs = {} # key: symbol, value: price , time
buyPairs = {}  # key: symbol, value: price , time
highestPricePairs = {} # key: symbol, value: price , time
lowestPricePairs = {}
statistic = {}
statisticBySellTime = {}
statisticByBuyTime = {}
successPairs = []
failedPairs = []
tradeVolumPairs = {}
farnTokenClubSymbols = ["LAZIOUSDT","PORTOUSDT","SANTOSUSDT","JUVUSDT","ATMUSDT","ASRUSDT","BARUSDT","OGUSDT","CITYUSDT","PSGUSDT","ACMUSDT"]

def getValueInDict(key,dictt):
    if key in dictt:
        return dictt[key]
    else:
        return None

def writeLog(text):
    dt_now = datetime.now()
    dt_string = dt_now.strftime("%d/%m/%Y %H:%M:%S")
    directory_name = dt_now.strftime("%d-%m-%Y")
    pathlib.Path("logs").mkdir(exist_ok=True) 
    pathlib.Path("logs/"+directory_name).mkdir(exist_ok=True) 
    with open("logs/"+directory_name + "/" + log_file_name + "-" + str(math.floor(dt_now.hour/4)) + ".txt", "a") as myfile:
        myfile.write(dt_string + ": " +text + "\n")

def getBallances():
    global currency_ballance
    if real_trade_mode is True:
        currency_ballance = math.floor(float(client.get_asset_balance(asset=currency_symbol)["free"]))
    writeLog("BALLANCE :" +" USDT:" + str(currency_ballance))

def marketSellReal(symbol,quantity):
    if quantity >= 1:
        client.order_market_sell(symbol=symbol,quantity=quantity)
        
def marketBuyReal(symbol,quantity):
    writeLog("MARKET_BUY "  + " quantity:" + str(quantity))
    if quantity >= 1 :
        client.order_market_buy(symbol=symbol,quantity=quantity)

def spotBuyReal(symbol,price,invest_money):
    quantity = math.floor(invest_money / price) - 1
    writeLog("SPOT_BUY " +" price: "+ str(price) + " totalPrice: " + str(invest_money) + " quantity:" + str(quantity))
    if quantity > 1 :
        client.order_limit_buy(symbol=symbol, quantity=quantity, price=price)

def spotSellReal(symbol,price,quantity):
    writeLog("SPOT_SELL " +" price: "+ str(price) + " quantity: " + str(quantity))
    client.order_limit_sell(symbol=symbol, quantity=quantity, price=price)
    
def cancelOrderReal(symbol,order_id):
    writeLog("CANCEL_ORDER " +" order_id: "+ str(order_id))
    client.cancel_order(symbol=symbol, orderId=order_id)

def marketBuy(symbol,price,trade_type,change,buy_sel_ratio,experimental):
    order_book = getOrderBook(symbol)
    total_bid_vol = order_book["total_bid_vol"]
    total_ask_vol = order_book["total_ask_vol"]
    firstAskPrice = order_book["firstAskPrice"]
    firstBidPrice = order_book["firstBidPrice"]
    quantity = math.floor(budget / firstAskPrice)
    
    if firstAskPrice/price < 0.996:
        writeLog("PRICE_DUMPING_NOT_BUYING " + symbol + " " + str(firstAskPrice) +" " + str(firstBidPrice) + " " + str(price))
        return

    if real_trade_mode is True and experimental is False:
        marketBuyReal(symbol,quantity)

    # writeLog(symbol + " " + str(getHistoryTradeBy5s(symbol)))
    print(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + ": BUY " + symbol + " Price: " + str(firstAskPrice) +" " + str(trade_type))
    writeLog("BUY_NOW "+symbol +" " + str(trade_type) +" currentPrice: "+ str(firstAskPrice) + " " + str(time.time()*1000))
    
    global totalProfit
    buyPairs[symbol] = {"price" : firstAskPrice, "time": time.time(), "verify_time" : time.time(), "verify_price" : firstAskPrice, "trade_type" : trade_type, "change" : change, "buy_sel_ratio" : buy_sel_ratio}
    highestPricePairs[symbol] = {"price" : firstAskPrice, "time": time.time(), "profit" : 0}
    time.sleep(0.5)
    getBallances()


def getAssetQuantity(symbol):
    if real_trade_mode is True:
        assetInfo = client.get_asset_balance(asset=symbol.replace(currency_symbol,""))
        return (math.floor(float(assetInfo["free"]))) if assetInfo is not None else 0
    else:
        return 1

def marketSell(why,symbol,buyPrice,buyPriceTime,price,highestPrice,lowestPrice,quantity):    
    if real_trade_mode is True:
        marketSellReal(symbol,quantity)
    profit = round(price/buyPrice,3)*100 - 100 - 0.2

    writeLog(why + " Sell "+symbol +" buyPrice: "+ str(buyPrice) +" currentPrice: "+ str(price) +" highest: " + str(highestPrice)+ " lowest: " + str(lowestPrice) + " CurrentVolIn5m " +str(getCurrentVolIn5m(symbol)) +" Profit: " + str(profit))
   
    global totalProfit
    global timed_out

    totalProfit = totalProfit + profit

    if len(buyPairs) == max_coin_hold:
        writeLog("COOLDOWN_AFTER_TRADES ")
        timed_out = time.time() + 60 * 15

    if getValueInDict(symbol,tradingPairs) is not None:
        tradingPairs.pop(symbol)
    if getValueInDict(symbol,buyPairs) is not None:
        buyPairs.pop(symbol)
    if getValueInDict(symbol,highestPricePairs) is not None:
        highestPricePairs.pop(symbol)
    if getValueInDict(symbol,lowestPricePairs) is not None:
        lowestPricePairs.pop(symbol)

    global totalTrades
    global totalWonTrades


    if price > buyPrice:
        totalWonTrades = totalWonTrades + 1
        successPairs.append(symbol)
    else:
        failedPairs.append(symbol)
    totalTrades = totalTrades + 1

    statCount = getValueInDict(why,statistic)
    if statCount is None:
        statCount = 1
    else:
        statCount = statCount + 1
    statistic[why] = statCount

    sellTime = str(datetime.now().minute % 15)
    profitBySellTime = getValueInDict(sellTime,statisticBySellTime)
    if profitBySellTime is None:
        profitBySellTime = profit
    else:
        profitBySellTime = profitBySellTime + profit
    statisticBySellTime[sellTime] = profitBySellTime

    buyTime = str(int(buyPriceTime) % 15)
    profitByBuyTime = getValueInDict(buyTime,statisticByBuyTime)
    if profitByBuyTime is None:
        profitByBuyTime = profit
    else:
        profitByBuyTime = profitByBuyTime + profit
    statisticByBuyTime[buyTime] = profitByBuyTime

    excludedPairs[symbol] = {"price" : price, "time": (time.time() + 60*20)}
    print(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + ": SELL " + symbol + " Price: " + str(price) + " Profit: " + str(profit))
    print(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + ": TOTAL: " + str(totalProfit))
    writeLog("TOTAL_PROFIT: " + str(totalProfit) +", Winrate: " + str(round(totalWonTrades/totalTrades*100,1))+" " + str(totalWonTrades)+"/"+ str(totalTrades) )
    writeLog("STATS: " + str(statistic))
    writeLog("STATS_BY_SELL_TIME: " + str(statisticBySellTime))
    writeLog("STATS_BY_BUY_TIME: " + str(statisticByBuyTime))
    writeLog("SUCCESS_PAIRS: " + str(successPairs))
    writeLog("FAILED_PAIRS: " + str(failedPairs))
    writeLog("EXCLUDED_PAIRS: " + str(excludedPairs))
    time.sleep(0.4)
    getBallances()

def isSymbolUp(symbol):
    candles = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_15MINUTE, "1 hour ago")
    candlesLen = len(candles)
    if candlesLen > 0 :
        lastcandle = candles[candlesLen-1]
        if float(lastcandle[4]) - float(lastcandle[1]) > 0 :
            return True
        else:
            return False
    else :
        return False
def isBTCPumpingDown():
    candles = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_15MINUTE, "1 hour ago")
    candlesLen = len(candles)
    if candlesLen > 1 :
        candles = list(reversed(candles))
        lastcandle = candles[0]
        writeLog("BTC_PRICE O:" + str(lastcandle[1]) + " H:" + str(lastcandle[2]) + " L:" + str(lastcandle[3]) + " C:"  + str(lastcandle[4])  + " C/O:" + str(float(lastcandle[4]) / float(lastcandle[1])))
        
        currentPrice = float(lastcandle[4])
        lowestPrice = 0.0
        highestPrice = 0.0
        hasATallCandle = False
        for candle in candles:
            v_o = float(candle[1])
            v_h = float(candle[2])
            v_l = float(candle[3])
            v_c = float(candle[4])
            if v_c / v_o > 1.0022:
                break
            if lowestPrice == 0.0:
                lowestPrice = v_c
            elif v_c < lowestPrice:
                lowestPrice = v_c
            if highestPrice == 0.0:
                highestPrice = v_o
            elif v_o > highestPrice:
                highestPrice = v_o
            if v_c / v_o < 0.9986:
                hasATallCandle = True
        if float(lastcandle[4]) / float(lastcandle[1]) < 1.0014 and hasATallCandle is True  and ((highestPrice >0  and lowestPrice / highestPrice < 0.994) or float(lastcandle[4]) / float(lastcandle[1]) <= 0.995  ):
            return True
        else:
            return False
    else :
        return False

def getSymbolInfo(symbol):
    candles = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_5MINUTE, "1 hour ago")
    candlesLen = len(candles)
    if candlesLen > 4 :
        candles = list(reversed(candles))
        currentCandle = candles[0]
        candle1 = candles[1]
        candle2 = candles[2]
        candle3 = candles[3]

        body1= float(currentCandle[4]) / float(currentCandle[1])
        if body1 < 1:
            body1 = 1/body1

        writeLog("GET_SYMBOL " + symbol +" " + str(float(currentCandle[4]) / float(currentCandle[1]))  + " " + str(float(candle2[2]) / float(candle2[3])) + " " + str(float(candle3[2]) / float(candle3[3])))
        if  float(candle1[7]) / budget > 20:
            return True
        else:
            return False
    else:
        return False

def filterInvalidCandle(candle):
    v_o = float(candle[1])
    v_h = float(candle[2])
    v_l = float(candle[3])
    v_c = float(candle[4])
    threshold = v_o / v_c
    if True:
        return True
    else:
        return False
def getOrderBook(symbol):
    depth = client.get_order_book(symbol=symbol)
    bids = depth["bids"]
    asks = depth["asks"]
    firstBid = bids[0]
    firstBidPrice = float(firstBid[0])
    firstBidQuantity = float(firstBid[1])

    firstAsk = asks[0]
    firstAskPrice = float(firstAsk[0])
    firstAskQuantity = float(firstAsk[1])

    total_bid_vol = 1
    for bid in bids:
        bid_price = float(bid[0])
        bid_quantity = float(bid[1])
        if bid_price/firstBidPrice < 1.02:
            total_bid_vol = total_bid_vol + bid_price*bid_quantity

    total_ask_vol = 1
    for ask in asks:
        ask_price = float(ask[0])
        ask_quantity = float(ask[1])
        if ask_price/firstBidPrice < 1.02:
            total_ask_vol = total_ask_vol + ask_price*ask_quantity
    return {"total_bid_vol" : total_bid_vol, "total_ask_vol":total_ask_vol, "firstAskPrice": firstAskPrice, "firstBidPrice" : firstBidPrice}

def getTradeVolumInfo(symbol):
    trades = client.aggregate_trade_iter(symbol=symbol, start_str = '5 minutes ago')
    buy_vol = 1
    buy_oders = 1
    sell_vol = 1
    sell_orders = 1

    for trade in trades:
        is_sell_order = trade["m"] is True
        trade_price = float(trade["p"])
        trade_quantity = float(trade["q"])

        if is_sell_order is True:
            sell_vol = sell_vol + trade_price*trade_quantity
            sell_orders = sell_orders + 1
        else:
            buy_vol = buy_vol + trade_price*trade_quantity
            buy_oders = buy_oders + 1

    return {"buy_vol":buy_vol , "buy_oders": buy_oders, "sell_vol":sell_vol, "sell_orders":sell_orders}

def getHistoryTradeBy5s(symbol):
    current_time = round(time.time()*1000)
    trades = list(reversed(list(client.aggregate_trade_iter(symbol=symbol, start_str = '5 minutes ago'))))
    history_trade = {}
    for trade in trades:
        is_sell_order = trade["m"] is True
        trade_time = float(trade["T"])
        trade_price = float(trade["p"])
        trade_quantity = float(trade["q"])
        second_since_current = math.floor((current_time - trade_time)/1000)
        pos = str(math.floor(second_since_current / 10) * 10)
        history_trade_item = history_trade[pos] if pos in history_trade else {}
        if is_sell_order is True:
            history_trade_item["sell_vol"] = (history_trade_item["sell_vol"] if "sell_vol" in history_trade_item else 0) + trade_price*trade_quantity
            history_trade_item["sell_orders"] = (history_trade_item["sell_orders"] if "sell_orders" in history_trade_item else 0) + 1
        else:
            history_trade_item["buy_vol"] = (history_trade_item["buy_vol"] if "buy_vol" in history_trade_item else 0) + trade_price*trade_quantity
            history_trade_item["buy_oders"] = (history_trade_item["buy_oders"] if "buy_oders" in history_trade_item else 0) + 1

        history_trade[pos] = history_trade_item
    return history_trade
 
def isLastMinute():
    return datetime.now().second > 45 and datetime.now().minute % 5 == 4

def conditionsSastified(symbol,priceThreshold,secondLapsed,minusSecondLapsed):
    candles5m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_5MINUTE, "2 hours ago")
    candles5m = list(reversed(candles5m))

    if len(candles5m) < 10:
        writeLog("NO_rANGE_AVOID " + symbol)
        permanentExcludedPairs[symbol] = {"price" : 0, "time": (time.time() + 60*20)}
        return None

    candles5m_vol_0 = float(candles5m[0][7]) if float(candles5m[0][7]) !=0 else 1
    candles5m_vol_1 = float(candles5m[1][7]) if float(candles5m[1][7]) !=0 else 1
    candles5m_vol_2 = float(candles5m[2][7]) if float(candles5m[2][7]) !=0 else 1
    candles5m_vol_3 = float(candles5m[3][7]) if float(candles5m[3][7]) !=0 else 1
    candles5m_vol_4 = float(candles5m[4][7]) if float(candles5m[4][7]) !=0 else 1
    candles5m_vol_5 = float(candles5m[5][7]) if float(candles5m[5][7]) !=0 else 1
    candles5m_change_0 = float(candles5m[0][4])/float(candles5m[0][1]) - 0.9999
    candles5m_change_1 = float(candles5m[1][4])/float(candles5m[1][1]) - 0.9999
    candles5m_change_2 = float(candles5m[2][4])/float(candles5m[2][1]) - 0.9999
    candles5m_change_3 = float(candles5m[3][4])/float(candles5m[3][1]) - 0.9999
    candles5m_change_4 = float(candles5m[4][4])/float(candles5m[4][1]) - 0.9999
    candles5m_v_c_0 = float(candles5m[0][4])
    candles5m_v_o_0 = float(candles5m[0][1])

    candles5m_consecutive_green = 0
    candles5m_consecutive_green_amp = 0
    candles5m_consecutive_high_green = 0
    candles5m_amp_last_30m = 0
    candles5m_very_high_amp = 0
    candles5m_very_high_vol = 0
    candles5m_high_amp_last_30m = 0
    candles5m_consecutive_green_boolean = True
    candles5m_consecutive_green_low_amp = True
    kick_offs_5m = 0

    # {"type" : "tip/dip", "v_c" : 0.3, "candle_index" :0}
    dip_array =[{"v_c" : candles5m_v_c_0, "candle_index" :0}]
    tip_array =[{"v_c" : candles5m_v_c_0, "candle_index" :0}]

    lowest_from_last_tip_index = 0
    lowest_from_last_tip_v_c = candles5m_v_c_0
    highest_from_last_dip_index = 0
    highest_from_last_dip_v_c = candles5m_v_c_0

    candles5m_lowest_v_c = 0
    for index,candle in enumerate(candles5m):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        v_change = abs(v_c/v_o - 0.9999)
        if index <= 14 and candles5m_lowest_v_c == 0 or v_c < candles5m_lowest_v_c:
            candles5m_lowest_v_c = v_c
        if index > 0 and v_h/v_l > 1.02 and v_vol/candles5m_vol_0 > 0.5:
            candles5m_very_high_amp = candles5m_very_high_amp + 1
        if index <= 14 and index > 2:
            if v_h/v_l > 1.012 or ((v_h/v_l -1)/abs(candles5m_change_0) > 0.75 and abs(candles5m_change_0) > 0.02):
                candles5m_high_amp_last_30m = candles5m_high_amp_last_30m + 1
        if index > 0 and index <= 16 and v_vol/candles5m_vol_0 > 0.8:
            candles5m_very_high_vol = candles5m_very_high_vol + 1
        if candles5m_consecutive_green_boolean is True:
            if v_c/v_o >= 0.998:
                candles5m_consecutive_green = candles5m_consecutive_green + 1
                if v_c > v_o:
                    candles5m_consecutive_green_amp = candles5m_consecutive_green_amp + v_change
                    if v_c/v_o > 1.013 and index > 0:
                        candles5m_consecutive_high_green = candles5m_consecutive_high_green + 1
                else: 
                    candles5m_consecutive_green_amp = candles5m_consecutive_green_amp - v_change
                if index == 1:
                    if v_change > 0.02:
                        candles5m_consecutive_green_low_amp = False
                if index > 1:
                    if v_change/candles5m_change_0 > 0.4 and v_change/candles5m_change_0 < 1: 
                        candles5m_consecutive_green_low_amp = False
            else:
                candles5m_consecutive_green_boolean = False
        if index <= 6:
            if v_vol > 35000:
                kick_offs_5m = kick_offs_5m + 1
        if index <= 8:
            if v_c >= v_o:
                candles5m_amp_last_30m = candles5m_amp_last_30m + v_change
            else:
                candles5m_amp_last_30m = candles5m_amp_last_30m - v_change
        last_tip = tip_array[-1]
        last_tip_v_c = last_tip["v_c"]
        last_tip_candle_index = last_tip["candle_index"]
        second_last_tip = tip_array[-2] if len(tip_array) >= 2 else tip_array[-1]
        second_last_tip_v_c = second_last_tip["v_c"]
        second_last_tip_candle_index = second_last_tip["candle_index"]
        last_dip = dip_array[-1]
        last_dip_v_c = last_dip["v_c"]
        last_dip_candle_index = last_dip["candle_index"]
        second_last_dip = dip_array[-2] if len(dip_array) >= 2 else dip_array[-1]
        second_last_dip_v_c = second_last_dip["v_c"]
        second_last_dip_candle_index = second_last_dip["candle_index"]

        selected_tip_v = v_c if v_c > v_o else v_o
        if index - 3 > last_tip_candle_index:
            if last_dip_candle_index == 0 and last_dip_candle_index >= last_tip_candle_index:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
            #elif second_last_dip_candle_index > second_last_tip_candle_index and selected_tip_v > lowest_from_last_tip_v_c:
            elif selected_tip_v/lowest_from_last_tip_v_c >= 1.0075 and last_dip_candle_index >= last_tip_candle_index:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
        else:
            if lowest_from_last_tip_index > last_tip_candle_index and last_dip_candle_index >= last_tip_candle_index and selected_tip_v/lowest_from_last_tip_v_c > 1.005 and last_tip_v_c/lowest_from_last_tip_v_c > 1.005:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
            elif index > last_dip_candle_index and last_dip_candle_index > last_tip_candle_index and selected_tip_v/lowest_from_last_tip_v_c > 1.005:
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
            elif selected_tip_v > last_tip_v_c:
                tip_array.pop()
                tip_array.append({"v_c" : selected_tip_v, "candle_index" : index})
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
                

        last_tip = tip_array[-1]
        last_tip_v_c = last_tip["v_c"]
        last_tip_candle_index = last_tip["candle_index"]
        second_last_tip = tip_array[-2] if len(tip_array) >= 2 else tip_array[-1]
        second_last_tip_v_c = second_last_tip["v_c"]
        second_last_tip_candle_index = second_last_tip["candle_index"]
        last_dip = dip_array[-1]
        last_dip_v_c = last_dip["v_c"]
        last_dip_candle_index = last_dip["candle_index"]
        second_last_dip = dip_array[-2] if len(dip_array) >= 2 else dip_array[-1]
        second_last_dip_v_c = second_last_dip["v_c"]
        second_last_dip_candle_index = second_last_dip["candle_index"]

        selected_dip_v = v_c if v_c < v_o else v_o
        if index - 3 > last_dip_candle_index:
            if last_tip_candle_index == 0 and last_tip_candle_index >= last_dip_candle_index:
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
            #elif second_last_tip_candle_index > second_last_dip_candle_index and selected_dip_v < highest_from_last_dip_v_c:
            elif highest_from_last_dip_v_c/selected_dip_v >= 1.0075 and last_tip_candle_index >= last_dip_candle_index:
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
        else:
            if highest_from_last_dip_index > last_dip_candle_index and last_tip_candle_index >= last_dip_candle_index and highest_from_last_dip_v_c/selected_dip_v > 1.005 and highest_from_last_dip_v_c/last_dip_v_c > 1.005:
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
            elif index > last_tip_candle_index and last_tip_candle_index > last_dip_candle_index and highest_from_last_dip_v_c/selected_dip_v > 1.005:
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v
            elif selected_dip_v < last_dip_v_c:
                dip_array.pop()
                dip_array.append({"v_c" : selected_dip_v, "candle_index" : index})
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v

        if lowest_from_last_tip_index - 1 <= index:
            if selected_tip_v < lowest_from_last_tip_v_c :
                lowest_from_last_tip_index = index
                lowest_from_last_tip_v_c = selected_tip_v
        if highest_from_last_dip_index - 1 <= index:
            if selected_dip_v > highest_from_last_dip_v_c:
                highest_from_last_dip_index = index
                highest_from_last_dip_v_c = selected_dip_v

    dip_array.append({"v_c" : float(candles5m[len(candles5m)-1][4]), "candle_index" : len(candles5m)-1})
    tip_array.append({"v_c" : float(candles5m[len(candles5m)-1][4]), "candle_index" : len(candles5m)-1})

    for index,dip in enumerate(dip_array):
        writeLog("DIP " + symbol + " " + str(dip))

    for index,tip in enumerate(tip_array):
        writeLog("TIP " + symbol + " " + str(tip))

    tip_going_up = True
    tip_going_up_wo_0 = True
    dip_going_up = True
    for index,tip in enumerate(tip_array[:-1]):
        v_c = tip["v_c"]
        candle_index = tip["candle_index"]
        if tip_array[index+1]["candle_index"] < len(candles5m) - 3:
            if (v_c/tip_array[index+1]["v_c"] < 0.99 and candle_index != 0) or (candle_index == 0 and v_c/tip_array[index+1]["v_c"] < 0.97):
                tip_going_up = False
            if v_c/tip_array[index+1]["v_c"] < 0.99 and candle_index != 0:
                tip_going_up_wo_0 = False
    for index,dip in enumerate(dip_array[:-1]):
        v_c = dip["v_c"]
        candle_index = dip["candle_index"]
        if v_c/dip_array[index+1]["v_c"] < 0.997:
            if dip_array[index+1]["candle_index"] < len(candles5m) - 3:
                dip_going_up = False

    first_tip = tip_array[0]
    first_tip_v_c = first_tip["v_c"]
    first_tip_candle_index = first_tip["candle_index"]
    second_tip = tip_array[1 if len(tip_array) > 1 else 0]
    second_tip_v_c = second_tip["v_c"]
    second_tip_candle_index = second_tip["candle_index"]
    third_tip = tip_array[2 if len(tip_array) > 2 else 0]
    third_tip_v_c = third_tip["v_c"]
    third_tip_candle_index = third_tip["candle_index"]

    first_dip = dip_array[0]
    first_dip_v_c = first_dip["v_c"]
    first_dip_candle_index = first_dip["candle_index"]
    second_dip = dip_array[1 if len(dip_array) > 1 else 0]
    second_dip_v_c = second_dip["v_c"]
    second_dip_candle_index = second_dip["candle_index"]
    third_dip = dip_array[2 if len(dip_array) > 2 else 0]
    third_dip_v_c = third_dip["v_c"]
    third_dip_candle_index = third_dip["candle_index"]

    anchor_tip = first_tip if first_tip_candle_index >= 3 else second_tip
    for index,tip in enumerate(tip_array[:-1]):
        v_c = tip["v_c"]
        v_index = tip["candle_index"]
        if v_index > first_dip_candle_index and v_index < second_dip_candle_index:
            anchor_tip = tip
            break
    anchor_tip_v_c = anchor_tip["v_c"]
    anchor_tip_candle_index = anchor_tip["candle_index"]

    anchor_dip = first_dip if first_dip_candle_index >= 3 else second_dip
    for index,dip in enumerate(dip_array[:-1]):
        v_c = dip["v_c"]
        v_index = dip["candle_index"]
        if v_index >= anchor_tip_candle_index:
            anchor_dip = dip
            break
    anchor_dip_v_c = anchor_dip["v_c"]
    anchor_dip_candle_index = anchor_dip["candle_index"]

    
    rise_percentage_from_dip_v_c = round((candles5m_v_c_0 - second_dip_v_c)/abs(anchor_tip_v_c - second_dip_v_c),2) if anchor_tip_v_c != second_dip_v_c else 1
    rise_percentage_from_dip_v_o = round((first_dip_v_c - second_dip_v_c)/abs(anchor_tip_v_c - second_dip_v_c),2) if anchor_tip_v_c != second_dip_v_c else 1

    candles1m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1MINUTE, "1 hour ago")
    candles1m = list(reversed(candles1m))

    candles1m_vol_0 = float(candles1m[0][7]) if float(candles1m[0][7]) > 0 else 1
    candles1m_vol_1 = float(candles1m[1][7]) if float(candles1m[1][7]) > 0 else 1
    candles1m_vol_2 = float(candles1m[2][7]) if float(candles1m[2][7]) > 0 else 1
    candles1m_vol_3 = float(candles1m[3][7]) if float(candles1m[3][7]) > 0 else 1
    candles1m_vol_4 = float(candles1m[4][7]) if float(candles1m[4][7]) > 0 else 1
    candles1m_vol_5 = float(candles1m[5][7]) if float(candles1m[5][7]) > 0 else 1
    candles1m_vol_6 = float(candles1m[6][7]) if float(candles1m[6][7]) > 0 else 1
    candles1m_change_0 = float(candles1m[0][4])/float(candles1m[0][1]) - 0.9999
    candles1m_change_1 = float(candles1m[1][4])/float(candles1m[1][1]) - 0.9999
    candles1m_change_2 = float(candles1m[2][4])/float(candles1m[2][1]) - 0.9999
    candles1m_change_3 = float(candles1m[3][4])/float(candles1m[3][1]) - 0.9999
    candles1m_change_4 = float(candles1m[4][4])/float(candles1m[4][1]) - 0.9999
    candles3m_vol_0 = float(candles1m[0][7]) + float(candles1m[1][7]) + float(candles1m[2][7])
    candles3m_vol_1 = float(candles1m[3][7]) + float(candles1m[4][7]) + float(candles1m[5][7])
    high_vol_green_candles_5m = 0
    high_vol_green_candles_10m = 0
    high_vol_red_candles_5m = 0
    high_amp_red_candles_5m = 0
    high_vol_red_candles_10m = 0
    kick_offs_1m = 0
    candles1m_highest_vol_in_10m = 0
    candles1m_green_amp_higher_than_05_10m = 0
    for index,candle in enumerate(candles1m[:60]):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        v_change = abs(v_c/v_o - 0.9999)
        compared_to_vol_0 = False
        if float(candles1m[0][7]) > 20000 and v_vol > 15000:
            if v_vol/float(candles1m[0][7]) > 0.3:
                compared_to_vol_0 = True
            else:
                compared_to_vol_0 = False
        if index <= 8 and v_vol > 1000:
            kick_offs_1m = kick_offs_1m + 1
        if index < 10:
            if v_vol > candles1m_highest_vol_in_10m:
                candles1m_highest_vol_in_10m = v_vol
        if v_vol > 1500 and compared_to_vol_0 is True:
            if index>= 0 and index <= 5 and v_c/v_o >= 1.00:
                high_vol_green_candles_5m = high_vol_green_candles_5m + 1
            if index>= 6 and index <= 10 and v_c/v_o >= 1.00:
                high_vol_green_candles_10m = high_vol_green_candles_10m + 1
            if index>= 0 and index <= 5 and v_o/v_c > 1.002:
                high_vol_red_candles_5m = high_vol_red_candles_5m + 1
            if index>= 6 and index <= 10 and v_o/v_c > 1.002:
                high_vol_red_candles_10m = high_vol_red_candles_10m + 1
        if index>= 0 and index <= 5 and v_o > v_c and (v_change/candles1m_change_0 > 0.7 or (v_change/candles1m_change_0 > 0.3 and (v_h/v_l-1)/candles1m_change_0 > 0.9)):
            high_amp_red_candles_5m = high_amp_red_candles_5m + 1
        if index <= 10 and v_c/v_o > 1.005:
            candles1m_green_amp_higher_than_05_10m = candles1m_green_amp_higher_than_05_10m + 1

    line_a = anchor_tip_v_c/first_dip_v_c - 0.9999
    line_b = anchor_tip_v_c/second_dip_v_c - 0.9999

    green_count_1m = 0
    candles1m_high_amp_low_change = 0
    candles1m_highest_red_index= -1
    candles1m_high_amp_10m = 0

    for index,candle in enumerate(candles1m[:10]):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        v_change = abs(v_c/v_o - 0.9999)
        if v_vol/candles1m_highest_vol_in_10m > 0.3 and v_vol > 3000:
            green_count_1m = green_count_1m + 1
        if v_h/v_l > 1.01 and v_change/(v_h/v_l - 0.9999) < 0.5:
            candles1m_high_amp_low_change = candles1m_high_amp_low_change + 1
        if v_o > v_c:
            if candles1m_highest_red_index == -1 or float(candles1m[candles1m_highest_red_index][2])/float(candles1m[candles1m_highest_red_index][3]) < v_h/v_l:
                candles1m_highest_red_index = index
        if v_h/v_l > 1.005:
            candles1m_high_amp_10m = candles1m_high_amp_10m + 1

    writeLog("CONDITION_CHECK " + symbol + " tip_going_up " + str(tip_going_up) + " tip_going_up_wo_0 " + str(tip_going_up_wo_0)+ " dip_going_up " + str(dip_going_up) +" candles5m_consecutive_green " + str(candles5m_consecutive_green) + " candles5m_consecutive_high_green " + str(candles5m_consecutive_high_green) + " candles5m_consecutive_green_low_amp " + str(candles5m_consecutive_green_low_amp))
    writeLog(symbol + " high_vol_green_candles_5m " + str(high_vol_green_candles_5m) + " high_vol_green_candles_10m " + str(high_vol_green_candles_10m) + " high_vol_red_candles_5m " + str(high_vol_red_candles_5m) +" high_amp_red_candles_5m " + str(high_amp_red_candles_5m) + " high_vol_red_candles_10m " + str(high_vol_red_candles_10m))
    writeLog(symbol +" rise_percentage_from_dip_v_c " + str(rise_percentage_from_dip_v_c) +" rise_percentage_from_dip_v_o " + str(rise_percentage_from_dip_v_o) + " candles5m_consecutive_green_amp " + str(candles5m_consecutive_green_amp) + " candles5m_amp_last_30m " + str(candles5m_amp_last_30m) +" candles5m_high_amp_last_30m " + str(candles5m_high_amp_last_30m) + " candles5m_very_high_amp " + str(candles5m_very_high_amp) + " candles5m_very_high_vol " + str(candles5m_very_high_vol))
    writeLog(symbol + " line_a " + str(line_a) + " line_b " + str(line_b) + " candles1m_high_amp_low_change " + str(candles1m_high_amp_low_change) + " candles1m_high_amp_10m " + str(candles1m_high_amp_10m) + " candles1m_highest_red_index " + str(candles1m_highest_red_index) + " candles1m_green_amp_higher_than_05_10m " + str(candles1m_green_amp_higher_than_05_10m))
    writeLog(symbol + " candles5m_vol_0 " + str(candles5m_vol_0) + " candles3m_vol_0 " + str(candles3m_vol_0) + " candles3m_vol_1 " + str(candles3m_vol_1) + " candles1m_vol_0 " + str(candles1m_vol_0) + " candles1m_highest_vol_in_10m " + str(candles1m_highest_vol_in_10m))
    writeLog(symbol + " vc/vo_5m " + str(float(candles5m[0][4])/float(candles5m[0][1])) + " vh/v_l_5m " + str(float(candles5m[0][2])/float(candles5m[0][3])) + " vc/vo_1m " + str(float(candles1m[0][4])/float(candles1m[0][1])) + " vh/vl_1m " + str(float(candles1m[0][2])/float(candles1m[0][3])))
    writeLog(symbol + " candles5m_change_0 " + str(candles5m_change_0) + " candles5m_change_1 " + str(candles5m_change_1) + " candles5m_change_2 " + str(candles5m_change_2) + " candles1m_change_0 " + str(candles1m_change_0) + " candles1m_change_1 " + str(candles1m_change_1) + " candles1m_change_2 " + str(candles1m_change_2))
    writeLog(symbol +" anchor_tip " + str(anchor_tip) + " anchor_dip " + str(anchor_dip) +" green_count_1m " + str(green_count_1m) + " kick_offs_1m " + str(kick_offs_1m) + " kick_offs_5m " + str(kick_offs_5m))

    if len(tip_array) + len(dip_array) > 12:
        writeLog("TOO_MANY_WAVES " + symbol + " " + str(candles5m_lowest_v_c))
        return None
    if (float(candles1m[0][2])/float(candles1m[0][3]) - 1)/abs(candles1m_change_0) < 0.6 or (float(candles5m[0][2])/float(candles5m[0][3]) -1)/ abs(candles5m_change_0) < 0.6:
        writeLog("CHANGE/AMP_TOO_LOW")
        return None
    
    accelerate_count = 0
    accelerate_v_c = 0
    start_accelerate_v_c = 0
    for index,price in enumerate(getValueInDict(symbol,tradingPairs)["prices"][-10:]):
            if accelerate_v_c == 0:
                accelerate_v_c = price
                start_accelerate_v_c = price
            elif price > accelerate_v_c:
                accelerate_v_c = price
                accelerate_count = accelerate_count + 1
    candles5m_0_highest = True
    for index,candle in enumerate(candles5m[1:]):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        if v_vol/candles5m_vol_0 > 0.5:
            candles5m_0_highest = False

    writeLog(symbol + " accelerate_count " + str(accelerate_count) + " candles5m_0_highest " + str(candles5m_0_highest) + " start_accelerate_v_c " + str(accelerate_v_c/start_accelerate_v_c))
    if dip_going_up is True and accelerate_count >= 4 and candles5m_0_highest is True and green_count_1m <= 5 and kick_offs_5m <= 5 and ((candles5m_vol_0/candles5m_vol_1 > 2 and candles5m_change_0/abs(candles5m_change_1) > 3 and candles5m_change_0 > 0.022 and candles5m_vol_0 > 30000 and candles1m_change_0 > 0.02) or (candles1m_vol_0/candles1m_vol_1 > 2 and abs(candles1m_change_0)/abs(candles1m_change_1) > 3 and candles1m_change_0 > 0.025 and candles1m_vol_0 > 34000) or (datetime.now().second <= 10 and candles1m_change_0 > 0.003 and candles1m_change_0 < 0.0125 and (abs(candles1m_change_0) + abs(candles1m_change_1))/abs(candles1m_change_2) > 5 and candles1m_vol_1/candles1m_vol_2 > 1 and candles1m_change_1 > 0.02)) and candles1m_high_amp_10m <= 3 and accelerate_v_c/start_accelerate_v_c > 1.007 and candles5m_consecutive_green_amp < 0.065 and candles5m_consecutive_green_amp > 0.03 and (candles5m_change_0 > 0.02 or candles1m_change_0/candles5m_change_0 > 0.8) and rise_percentage_from_dip_v_c >= 1 and candles5m_amp_last_30m > 0 and line_a > 0 and line_b > 0 and line_a < 0.09 and line_b < 0.09 and candles5m_consecutive_green_amp/candles5m_amp_last_30m > 0.75 and candles1m_high_amp_low_change <= 2 and (second_tip_candle_index >= 5 or second_tip_candle_index == 0) and datetime.now().second <= 50 and candles5m_very_high_amp <= 1 and candles5m_very_high_vol <= 1 and candles5m_change_0 < 0.035:
        writeLog("TRADE_INFO_C " + symbol)
        return {"trade_type" : "TRADE_INFO_C", "change" : 0.01, "buy_sel_ratio" : 2, "experimental": True}
    if accelerate_count >= 6 and candles5m_0_highest is True and candles5m_change_0 > 0.01 and candles1m_change_0 > 0.015 and candles1m_vol_0/candles1m_vol_1 > 2 and datetime.now().second <= 55 and candles5m_very_high_amp <= 1 and candles1m_vol_0 > 30000 and candles5m_very_high_vol <= 1 and candles5m_change_0 < 0.04 and candles1m_change_0 < 0.04 and candles5m_consecutive_green_amp/candles5m_amp_last_30m > 0.75 and candles5m_consecutive_green_amp < 0.06 and candles5m_amp_last_30m < 0.06:
        writeLog("TRADE_INFO_C1 " + symbol)
        return {"trade_type" : "TRADE_INFO_C1", "change" : 0.01, "buy_sel_ratio" : 2, "experimental": True}
    if rise_percentage_from_dip_v_o < 0.9 and rise_percentage_from_dip_v_c - rise_percentage_from_dip_v_o < 0.15 and line_b > 0.05 and anchor_tip_candle_index >= first_dip_candle_index and anchor_tip_candle_index < second_dip_candle_index and line_a/line_b < 0.95 and line_a > 0 and line_b > 0 and ((float(candles5m[0][4])/float(candles5m[0][1]) < 1.015) or (float(candles5m[0][4])/float(candles5m[0][3]) > 1.02 and float(candles5m[0][1])/float(candles5m[0][4]) > 1 and candles5m_vol_1 < candles5m_vol_2)) and tip_going_up_wo_0 is True and anchor_tip_candle_index >= 7 and candles5m_change_1 > -0.01 and candles1m_high_amp_low_change <= 2 and high_amp_red_candles_5m <= 2:
        qualified_f = False
        first_red_vol = 1
        second_red_vol = 1
        red_candle_count = 0
        for index,candle in enumerate(candles5m[:7]):
            v_o = float(candle[1])
            v_h = float(candle[2])
            v_l = float(candle[3])
            v_c = float(candle[4])
            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
            v_change = abs(v_c/v_o - 0.9999)
            if v_c/v_o > 1.01 and index > 0 and index <= 3:
                writeLog("TRADE_INFO_F1 " + str(index))
                qualified_f = True
            if v_o/v_c > 1 and index > 0:
                red_candle_count = red_candle_count + 1
                if first_red_vol == 1 :
                    first_red_vol = v_vol
                elif second_red_vol == 1:
                    second_red_vol = v_vol
        for index,tip in enumerate(tip_array):
            v_c = tip["v_c"]
            v_index = tip["candle_index"]
            if v_index > anchor_tip_candle_index:
                if (v_c - anchor_dip_v_c)/(anchor_tip_v_c - anchor_dip_v_c) > 0.3:
                    qualified_f = True
                    writeLog("TRADE_INFO_F2 " + str((v_c - anchor_dip_v_c)/(anchor_tip_v_c - anchor_dip_v_c)))
                break
        if first_red_vol > second_red_vol or red_candle_count <= 2 or float(candles5m[first_tip_candle_index][7])/second_red_vol < 2:
            writeLog("TRADE_INFO_F3 " + str(first_red_vol) + " " + str(second_red_vol) + " " + str(red_candle_count) + " " + str(float(candles5m[first_tip_candle_index][7])/second_red_vol))
            qualified_f = True
        writeLog("TRADE_INFO_F4 " + str(qualified_f) + " ")
        if qualified_f is False and candles5m_consecutive_green >= 2 and candles5m_change_0 > 0.005:
            writeLog("TRADE_INFO_F " + symbol)
            return {"trade_type" : "TRADE_INFO_F", "change" : 0.01, "buy_sel_ratio" : 2, "experimental": True}

    # if candles5m_v_c_0/candles5m_lowest_v_c > 1.09 or float(candles1m[0][2])/float(candles1m[0][3]) > 1.09 or float(candles5m[0][2])/float(candles5m[0][3]) > 1.09:
    #     writeLog("ALREADY_TOO_HIGH " + symbol + " " + str(candles5m_lowest_v_c))
    #     return None

    
    green_amp_higher_than_1_3m = 0
    green_amp_higher_than_1_3m_10m = 0
    marked_vol = float(candles1m[1][7]) if float(candles1m[1][7]) > float(candles1m[2][7]) else float(candles1m[2][7])
    marked_vol = float(candles1m[1][7]) if float(candles1m[1][7]) > float(candles1m[0][7]) else float(candles1m[0][7])
    marked_vol = marked_vol if marked_vol > 0 else 1
    for index,candle in enumerate(candles1m[:30]):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        v_change = abs(v_c/v_o - 0.9999)
        if index <= 2:
            if ((index == 0 and v_c/v_o >= 1.01) or (index != 0 and v_c/v_o >= 1.015)) and v_c/v_o < 1.02 and v_h/v_l < 1.025 and ((v_vol > 20000 and index == 0) or index > 0) and float(candles1m[index+1][7]) > 0 and v_vol/float(candles1m[index+1][7]) > 0.2 and v_vol < 250000:
                writeLog("TRADE_INFO_E_0 " + str(index) + " " + str(v_c) + " " + str(v_o) + " " + str(v_vol))
                green_amp_higher_than_1_3m = green_amp_higher_than_1_3m + 1
            elif v_change/candles1m_change_0 > 0.5 or (v_h/v_l-1)/candles1m_change_0 > 0.9 or v_vol/marked_vol > 0.5:
                writeLog("TRADE_INFO_E_1 " + str(index) + " " + str(v_h/v_l))
                green_amp_higher_than_1_3m = green_amp_higher_than_1_3m - 1
        else:
            if v_change/candles1m_change_0 > 0.9 or (v_change > 0.005 and v_h/v_l > 1.015) or v_h/v_l > 1.04 or (v_c/v_o -1)/abs(candles1m_change_0) > 0.9 or v_vol/candles1m_vol_0 > 0.8 or v_vol/marked_vol > 0.6:
                green_amp_higher_than_1_3m_10m = green_amp_higher_than_1_3m_10m + 1
                if index == 3 or index == 4:
                    writeLog("TRADE_INFO_E_2 " + str(index) +" " + str(v_vol) +" " + str(v_change) +" " + str(v_h/v_l))
                    green_amp_higher_than_1_3m = green_amp_higher_than_1_3m - 1
    writeLog(symbol +" green_amp_higher_than_1_3m " + str(green_amp_higher_than_1_3m) +" green_amp_higher_than_1_3m_10m " + str(green_amp_higher_than_1_3m_10m) + " marked_vol " + str(marked_vol))
    if green_amp_higher_than_1_3m == 2 and green_amp_higher_than_1_3m_10m <= 1 and candles1m_green_amp_higher_than_05_10m <= 4 and candles1m_change_0 > 0.008 and candles5m_consecutive_high_green <= 1 and priceThreshold < 1.07 and (rise_percentage_from_dip_v_c > 1.2 or rise_percentage_from_dip_v_c == 1 or len(tip_array) == 1) and candles5m_consecutive_green_amp < 0.065 and line_b < 0.08 and candles5m_amp_last_30m < 0.09 and (candles1m_vol_0 > 25000 or candles3m_vol_0 > 40000) and candles5m_very_high_amp <= 1 and candles5m_very_high_vol <= 1 and accelerate_count >= 3 and green_count_1m >= 2 and kick_offs_1m >= 4 and kick_offs_1m <= 7 and kick_offs_5m <= 5 and candles1m_high_amp_low_change <= 2 and high_amp_red_candles_5m <= 1 and candles5m_high_amp_last_30m <= 1:
        return {"trade_type" : "TRADE_INFO_E", "change" : 0.01, "buy_sel_ratio" : 2, "experimental": True}

    candles1m_mini_green_count = 0
    candles1m_mini_red_count = 0
    candles1m_mini_red_count_1 = 0
    candles1m_mini_green_count_1 = 0
    k_stopper = 0
    highest_vol_index = 0
    highest_vol_value = 0
    for index,candle in enumerate(candles1m[:45]):
            v_o = float(candle[1])
            v_h = float(candle[2])
            v_l = float(candle[3])
            v_c = float(candle[4])
            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
            v_change = abs(v_c/v_o - 0.9999)
            if index <= 7:
                if v_vol > 2000:
                    if v_c/v_o >= 1.002 and v_c/v_o <= 1.008:
                        candles1m_mini_green_count = candles1m_mini_green_count + 1
                    elif (v_c/v_o > 1.008):
                        candles1m_mini_green_count_1 = candles1m_mini_green_count_1 + 1
                if v_o/v_c >= 1.001 and v_h/v_l >= 1.002:
                    candles1m_mini_red_count = candles1m_mini_red_count + 1
                if v_o/v_c >= 1.001 and v_h/v_l > 1.01:
                    candles1m_mini_red_count_1 = candles1m_mini_red_count_1 + 1
                if v_h/v_l > 1.015 and v_c/v_o > 1.005:
                    k_stopper = k_stopper + 3
                elif v_h/v_l > 1.01 and v_o/v_c > 1.005:
                    k_stopper = k_stopper + 1
                if highest_vol_value == 0 or v_vol > highest_vol_value:
                    highest_vol_value = v_vol
                    highest_vol_index = index
            else:
                if v_h/v_l > 1.01 and v_c/v_o > 1.005:
                    k_stopper = k_stopper + 3
                elif v_h/v_l > 1.01 and v_o/v_c > 1.005:
                    k_stopper = k_stopper + 1
    
    writeLog("TRADE_INFO_K " + str(k_stopper) + " " + str(candles1m_mini_green_count) + " " + str(candles1m_mini_green_count_1) + " " + str(candles1m_mini_red_count) + " " + str(candles1m_mini_red_count_1) + " " + str(highest_vol_index))
    if k_stopper <= 0 and (candles1m_mini_green_count + candles1m_mini_green_count_1) >= 5 and candles1m_mini_green_count_1 >= 1 and candles1m_mini_green_count_1 <= 2 and candles1m_mini_red_count <= 1 and candles1m_mini_red_count_1 == 0 and candles5m_change_0 > 0.01 and candles1m_change_0 > 0.005 and candles5m_consecutive_green_amp < 0.045 and tip_going_up is True and abs(candles5m_change_1) < 0.015 and abs(candles5m_change_2) < 0.0125 and candles5m_change_0 < 0.03 and kick_offs_5m <= 6 and (candles1m_vol_1 + candles1m_vol_2)/(candles1m_vol_3 + candles1m_vol_4) > 0.9 and (candles1m_vol_3 + candles1m_vol_4)/(candles1m_vol_5 + candles1m_vol_6) > 0.9:
        return {"trade_type" : "TRADE_INFO_K", "change" : 0.01, "buy_sel_ratio" : 2, "experimental": True}
    return None

def fantokenConditionsSastified(symbol):
    candles5m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_5MINUTE, "2 hours ago")
    candles5m = list(reversed(candles5m))

    green_count = 0
    green_change = 0
    historical_green_count = 0
    historical_red_count = 0

    highest_v_c = 0
    highest_v_c_index = -1
    lowest_v_c = 0
    lowest_v_c_index = -1

    high_amp = False
    for index,candle in enumerate(candles5m):
        v_o = float(candle[1])
        v_h = float(candle[2])
        v_l = float(candle[3])
        v_c = float(candle[4])
        v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
        if index < 4:
            if v_c/v_o > 1:
                green_count = green_count + 1
                green_change = green_change + v_c/v_o -1
        if index <= 8 and v_h/v_l > 1.033:
            high_amp = True
        if highest_v_c == 0 or v_c > highest_v_c:
            highest_v_c = v_c
            highest_v_c_index = index
        if lowest_v_c == 0 or v_c < lowest_v_c:
            lowest_v_c = v_c
            lowest_v_c_index = index
        if v_c >= v_o:
            historical_green_count = historical_green_count + 1
        else:
            historical_red_count = historical_red_count + 1
    writeLog("FANTOKEN_CHECK " + symbol + " " + str(high_amp) + " " + str(green_count) + " " + str(green_change) + " " + str(lowest_v_c_index) + " " + str(lowest_v_c) + " " + str(highest_v_c_index) + " " + str(highest_v_c) + " " + str(float(candles5m[0][4])/float(candles5m[0][1])))
    if high_amp is False and highest_v_c/lowest_v_c < 1.06 and float(candles5m[0][4])/float(candles5m[0][1]) < 1.03 and green_change > 0.015 and lowest_v_c_index > 2:
        return True
    return False

def getCurrentVolIn5m(symbol):
    m5_candles = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1MINUTE, "10 minutes ago")
    if len(m5_candles) == 0:
        return 0
    else:
        total_vol = 0
        m5_candles = list(reversed(m5_candles))
        for index,candle in enumerate(m5_candles):
            v_o = float(candle[1])
            v_h = float(candle[2])
            v_l = float(candle[3])
            v_c = float(candle[4])
            v_vol = float(candle[7])
            if index < 5:
                total_vol = total_vol + v_vol
        return total_vol

def loopCheck(is_btc_down):
    global notified_at_minute
    if (datetime.now().minute % 15 == 0 and notified_at_minute != datetime.now().minute) or notified_at_minute == -1:
        notified_at_minute = datetime.now().minute
        print(datetime.now().strftime("%d/%m/%Y %H:%M:%S") +" " +" --- ")
    global timed_out
    global global_avoid_time

    if time.time() < timed_out :
        writeLog("TIMED_OUT")
    if is_btc_down is True:
        writeLog("BTC_DOWN BTC is going down !!")
        if timed_out < time.time() :
            writeLog("TIME_OUT")
            timed_out = time.time() + btc_down_timed_out
        tradingPairs.clear()     
    allPricesJson = client.get_all_tickers()
    totalHoldingProfit = 0.0
    notable_list = []
    for pair in allPricesJson:
        symbol = pair["symbol"]
        price = float(pair["price"])
        permanentExcluded = False if getValueInDict(symbol,permanentExcludedPairs) is None else True
        excludedDict = getValueInDict(symbol,excludedPairs)
        excluded_time = None if excludedDict is None else excludedDict["time"]
        excluded_price = None if excludedDict is None else excludedDict["price"]
        excluded = (excludedDict is not None and (excluded_time is not None and time.time() < excluded_time)) or permanentExcluded is True
        below500 = False
        if "e" in str(price):
            if int(str(price).split("e")[0].replace(".","")) < 100:
                below500 = True
        else:
            if int(str(price).replace(".","")) < 100:
                below500 = True
        if symbol.endswith("USDT") and price < max_crypto_price_allowed and price > 0 and permanentExcluded is False: 
            tradingPairDict = getValueInDict(symbol,tradingPairs)
            prices = [] if tradingPairDict is None else tradingPairDict["prices"]

            prices.append(price)
            if len(prices) > 900:
                prices.pop(0)

            tradingPairs[symbol] = {"prices" : prices}

            if prices is not None:
                if global_avoid_time != 0 and global_avoid_time < time.time():
                    #print(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S") +" GLOBAL_AVOID_END")) 
                    writeLog(str(datetime.now().strftime("%d/%m/%Y %H:%M:%S") +" GLOBAL_AVOID_END"))
                    global_avoid_time = 0
                if getValueInDict(symbol,buyPairs) is  None and ("SUPER" in symbol or ("DOWN" not in symbol and "UP" not in symbol)) and below500 is False:
                    pricesLen = len(prices)
                    if pricesLen >= 205:
                        lowestPrice = min(prices)
                        lastNPrices = prices[-200:]

                        highestPriceIn1m = 0
                        lowestPriceIn1m = 0
                        lowestPriceIn1mIndex = -1
                        secondLapsed_1 = 1
                        minusSecondLapsed = 1
                        thresholdLapsed = 0
                        minusThresholdLapsed = 0
                        lastNPricesReversed = list(reversed(lastNPrices))
                        for index,priceN in enumerate(lastNPricesReversed):
                            if lowestPriceIn1mIndex == -1 or priceN < lowestPriceIn1m:
                                lowestPriceIn1mIndex = index
                                lowestPriceIn1m = priceN
                            if highestPriceIn1m == 0 or priceN > highestPriceIn1m:
                                highestPriceIn1m = priceN

                        lastNPricesReversedTilLowest = lastNPricesReversed[:lowestPriceIn1mIndex + 1]
                        for index,priceN in enumerate(lastNPricesReversedTilLowest):
                            if index < len(lastNPricesReversedTilLowest) -1:
                                if priceN - lastNPricesReversedTilLowest[index + 1] > 0:
                                    secondLapsed_1 = secondLapsed_1 + 1
                                    thresholdLapsed = thresholdLapsed + priceN/lastNPricesReversedTilLowest[index + 1] - 1
                                elif priceN - lastNPricesReversedTilLowest[index + 1] < 0:
                                    minusSecondLapsed = minusSecondLapsed + 1
                                    minusThresholdLapsed = minusThresholdLapsed + lastNPricesReversedTilLowest[index + 1]/priceN - 1
                        
                        secondLapsed = lowestPriceIn1mIndex + 1
                        priceThreshold = price/lowestPriceIn1m
                        
                        if priceThreshold != 0:
                            if notable_threshold_alert > 1 and price/lowestPrice > 1.06:
                                writeLog("MISSED_OPPOTUNITY " + symbol + " " +str(price/lowestPrice) + " " +str(price))
                            if (notable_threshold_alert > 1 and priceThreshold > notable_threshold_alert) or (notable_threshold_alert < 1 and priceThreshold < notable_threshold_alert):
                                writeLog("NOTABLE_THRESHOLD " +" " + str(round(priceThreshold,3)) + " " + symbol +" "+ str(price) +" "+ str(lowestPriceIn1m) +" "+ str(lowestPriceIn1mIndex)  +" secondsLapsed: "+ str(secondLapsed) + " " + str(secondLapsed_1) + " " + str(minusSecondLapsed) + " " + str(thresholdLapsed) + " " + str(minusThresholdLapsed))

                                # and global_avoid_time < time.time()
                                if is_btc_down is False and currency_ballance > budget and time.time() > timed_out and excluded is False:  
                                    notable_list.append({"symbol" : symbol,"price": price, "priceThreshold" : priceThreshold, "secondLapsed_1":secondLapsed_1,"minusSecondLapsed":minusSecondLapsed,"thresholdLapsed": thresholdLapsed,"minusThresholdLapsed": minusThresholdLapsed})
            
    # sort by priceThreshold
    notable_list.sort(key=operator.itemgetter('priceThreshold'), reverse=True)            
    fan_token_symbol_count = 0
    for index,notable_item in enumerate(notable_list):
        symbol = notable_item["symbol"]
        price = notable_item["price"]
        priceThreshold = notable_item["priceThreshold"]
        secondLapsed_1 = notable_item["secondLapsed_1"]
        minusSecondLapsed = notable_item["minusSecondLapsed"]
        thresholdLapsed = notable_item["thresholdLapsed"]
        minusThresholdLapsed = notable_item["minusThresholdLapsed"]
        if symbol in farnTokenClubSymbols and priceThreshold > 1.035:
            fan_token_symbol_count = fan_token_symbol_count + 1

    if len(notable_list) > 20 and len(buyPairs) < max_coin_hold:
        btc_candles5m = client.get_historical_klines("BTCUSDT", Client.KLINE_INTERVAL_5MINUTE, "1 hours ago")
        btc_candles5m = list(reversed(btc_candles5m))
        btc_has_high_amp = False
        btc_high_red_count = 0
        for index,candle in enumerate(btc_candles5m):
            v_o = float(candle[1])
            v_h = float(candle[2])
            v_l = float(candle[3])
            v_c = float(candle[4])
            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
            if index != 0 and v_h/v_o > 1.01:
                btc_has_high_amp = True
            if v_o/v_l > 1.0125:
                btc_high_red_count = btc_high_red_count + 1

        # BTC DUMP THEN PUMP
        if btc_has_high_amp is False and btc_high_red_count > 0 and float(btc_candles5m[0][4])/float(btc_candles5m[0][1]) > 1.007 and float(btc_candles5m[0][4])/float(btc_candles5m[0][1]) > float(btc_candles5m[1][4])/float(btc_candles5m[1][1]) and float(btc_candles5m[1][4])/float(btc_candles5m[1][1]) > 0.998 and float(btc_candles5m[1][4])/float(btc_candles5m[1][1]) < 1.006 and notable_list[4]["priceThreshold"] > 1.04:
            for index,notable_item in enumerate(notable_list):
                symbol = notable_item["symbol"]
                price = notable_item["price"]
                priceThreshold = notable_item["priceThreshold"]
                secondLapsed_1 = notable_item["secondLapsed_1"]
                minusSecondLapsed = notable_item["minusSecondLapsed"]
                thresholdLapsed = notable_item["thresholdLapsed"]
                minusThresholdLapsed = notable_item["minusThresholdLapsed"]
                if priceThreshold < 1.025 and len(buyPairs) < max_coin_hold:
                    marketBuy(symbol,price,"TRADE_INFO_X",0.025,2,True)
        # BTC PUMP
        if btc_has_high_amp is False and abs(float(btc_candles5m[0][4])/float(btc_candles5m[0][1])-1)/abs(float(btc_candles5m[1][4])/float(btc_candles5m[1][1])-1) > 3 and float(btc_candles5m[0][4])/float(btc_candles5m[0][1]) > 1.005 and float(btc_candles5m[1][4])/float(btc_candles5m[1][1]) < 1.003 and notable_list[2]["priceThreshold"] > 1.04:
            for index,notable_item in enumerate(notable_list):
                symbol = notable_item["symbol"]
                price = notable_item["price"]
                priceThreshold = notable_item["priceThreshold"]
                secondLapsed_1 = notable_item["secondLapsed_1"]
                minusSecondLapsed = notable_item["minusSecondLapsed"]
                thresholdLapsed = notable_item["thresholdLapsed"]
                minusThresholdLapsed = notable_item["minusThresholdLapsed"]
                if priceThreshold < 1.025 and len(buyPairs) < max_coin_hold:
                    marketBuy(symbol,price,"TRADE_INFO_Y",0.025,2,True)
    notable_list_count = 4 if fan_token_symbol_count < 2 else 7
    if len(buyPairs) == 1:
        notable_list_count = 2
    elif len(buyPairs) >= 2:
        notable_list_count = 1
    for index,notable_item in enumerate(notable_list[:notable_list_count]):
        symbol = notable_item["symbol"]
        price = notable_item["price"]
        priceThreshold = notable_item["priceThreshold"]
        secondLapsed_1 = notable_item["secondLapsed_1"]
        minusSecondLapsed = notable_item["minusSecondLapsed"]
        thresholdLapsed = notable_item["thresholdLapsed"]
        minusThresholdLapsed = notable_item["minusThresholdLapsed"]
        if fan_token_symbol_count >= 2 :
            if symbol in farnTokenClubSymbols and priceThreshold < 1.035 and priceThreshold > 1.02 and fantokenConditionsSastified(symbol) is True:
                if len(buyPairs) < max_coin_hold:
                    marketBuy(symbol,price,"TRADE_INFO_FAN_TOKEN",0.025,2,True)
        else:
            result = conditionsSastified(symbol,priceThreshold,secondLapsed_1,minusSecondLapsed)
            if result is not None and len(buyPairs) < max_coin_hold:
                if "trade_type" in result and "change" in result:
                    marketBuy(symbol,price,result["trade_type"],result["change"],result["buy_sel_ratio"],result["experimental"])
                    break
                else:
                    writeLog("FUCKING_ERROR " + symbol +" " + str(result))
                    print("ERROR")

    for symbol in buyPairs.copy():
        buyPriceDict = getValueInDict(symbol,buyPairs)
        tradeType = "" if buyPriceDict is None else buyPriceDict["trade_type"]
        change = 0.01 if buyPriceDict is None else buyPriceDict["change"]
        buy_sel_ratio = 3 if buyPriceDict is None else buyPriceDict["buy_sel_ratio"]
        buyPrice = 0 if buyPriceDict is None else buyPriceDict["price"]
        buyPriceTime = 0 if buyPriceDict is None else buyPriceDict["time"]
        verifyTime = 0 if buyPriceDict is None else buyPriceDict["verify_time"]
        verifyPrice = 0 if buyPriceDict is None else buyPriceDict["verify_price"]
        highestPricePairDict = getValueInDict(symbol,highestPricePairs)
        highestPrice = 0 if highestPricePairDict is None else  highestPricePairDict["price"]
        highestPriceTime = 0 if highestPricePairDict is None else  highestPricePairDict["time"]
        highestProfit = 0 if highestPricePairDict is None else highestPricePairDict["profit"]
        lowestPricePairDict = getValueInDict(symbol,lowestPricePairs)
        lowestPrice = 0 if lowestPricePairDict is None else  lowestPricePairDict["price"]
        lowestPriceTime = 0 if lowestPricePairDict is None else  lowestPricePairDict["time"]
        lowestProfit = 0 if lowestPricePairDict is None else lowestPricePairDict["profit"]
        
        if buyPriceDict is not None:
            depth = client.get_order_book(symbol=symbol)
            bids = depth["bids"]
            asks = depth["asks"]
            if len(bids) == 0 or len(asks) == 0:
                writeLog("NO_ORDER_BOOK " + symbol)
                return None
                
            firstBid = bids[0]
            firstBidPrice = float(firstBid[0])
            firstBidQuantity = float(firstBid[1])

            firstAsk = asks[0]
            firstAskPrice = float(firstAsk[0])
            firstAskQuantity = float(firstAsk[1])

            # trade_info = getTradeVolumInfo(symbol)
            # buy_vol = trade_info["buy_vol"]
            # buy_oders = trade_info["buy_oders"]
            # sell_vol = trade_info["sell_vol"]
            # sell_orders = trade_info["sell_orders"]

            asset_quantity = getAssetQuantity(symbol)

            profit = round(firstBidPrice/buyPrice,3)*100 - 100 -0.15
            totalHoldingProfit = totalHoldingProfit + profit
            writeLog("HOLDING " + symbol +" buyPrice: "+ str(buyPrice) +  " currentPrice: "+ str(firstBidPrice) +  " highestPrice: "+ str(highestPrice) + " lowestPrice " + str(lowestPrice) + " threshold: " + str(round(firstBidPrice/buyPrice,3)) + " Profit: " + str(profit))
            # writeLog(symbol+ " " + str(trade_info))
            if highestPrice == 0 or firstBidPrice > highestPrice:
                highestPricePairs[symbol] = {"price" : firstBidPrice, "time": time.time(), "profit" : profit}
                highestPrice = firstBidPrice
            if lowestPrice == 0 or firstBidPrice < lowestPrice:
                lowestPricePairs[symbol] = {"price" : firstBidPrice, "time": time.time(), "profit" : profit}
                lowestPrice = firstBidPrice
            #else:
            if (firstBidPrice * firstBidQuantity) > 0 and asset_quantity > 0:
                if firstBidPrice / buyPrice >= immediate_upper_threshhold_to_sell:
                    marketSell("GOTTEM",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                elif is_btc_down is True   and   firstBidPrice / buyPrice <= (1 - (1-immediate_lower_threshhold_to_sell)/2):
                    writeLog("PANIC_SELL_NOT_SELLING")
                    #marketSell("PANIC_SELL",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                elif time.time() - buyPriceTime >= max_hold_duration and firstBidPrice / buyPrice >= max_duration_price_to_sell and firstBidPrice / buyPrice <= price_falling_threshold_to_sell :
                    marketSell("REACHED_MAX_DURATION",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                elif time.time() - buyPriceTime >= max_hold_duration + 60*30:
                    marketSell("REACHED_MAX_DURATION_1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                elif tradeType in trade_info_long and time.time() - buyPriceTime >= min_hold_duration:
                    if firstBidPrice / buyPrice <= immediate_lower_threshhold_to_sell:
                        marketSell("BUSTED",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif firstBidPrice / buyPrice < 1.1 and firstBidPrice / buyPrice >= floor_threshold_to_sell and highestPrice/buyPrice - firstBidPrice/buyPrice > 0.015:  
                        marketSell("REACH_MAX_C",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif (highestPrice - buyPrice) !=0 and (firstBidPrice- buyPrice) / (highestPrice - buyPrice) <= 0.85 and highestPrice / buyPrice >= floor_threshold_to_sell and firstBidPrice / buyPrice <= floor_threshold_to_sell:  
                        marketSell("OUT_HYPE_ZONE",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif buyPrice/lowestPrice > 1.03 and firstBidPrice/buyPrice > 1.002:
                        marketSell("REACH_MAX_D",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif highestPrice/buyPrice > 1.025 and firstBidPrice/buyPrice < 0.995:
                        marketSell("REACH_MAX_D1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif highestPrice/lowestPrice > 1.02 and highestPriceTime < lowestPriceTime and firstBidPrice/buyPrice > 1.002 and buyPrice/lowestPrice > 1.015 and highestPrice/buyPrice > 1.01:
                        marketSell("REACH_MAX_E",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    if time.time() - buyPriceTime > 60*3 and time.time() - buyPriceTime < 60*20 and firstBidPrice/buyPrice < 1.009 and firstBidPrice/buyPrice > 0.998:
                        minute_passed = round((time.time() - buyPriceTime)/60)
                        candles1m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1MINUTE, "20 minutes ago")
                        candles1m = list(reversed(candles1m))
                        red_candle_count = 0
                        candles1m_highest_index = 0
                        candles1m_second_highest_index = 0
                        below_v_c_count = 0
                        avg_amp = 0
                        avg_amp_count = 0
                        for index,candle in enumerate(candles1m):
                            v_o = float(candle[1])
                            v_h = float(candle[2])
                            v_l = float(candle[3])
                            v_c = float(candle[4])
                            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
                            v_change = abs(v_c/v_o - 0.9999)
                            if index < minute_passed:
                                if v_o/v_c > 1.001 or (v_o/v_c > 1 and v_h/v_l > 1.005):
                                    red_candle_count = red_candle_count + 1
                                elif v_o/v_c > 1 and v_h/v_l > 1.01:
                                    red_candle_count = red_candle_count + 2
                                if v_c/buyPrice < 0.997:
                                    below_v_c_count = below_v_c_count + 1
                            if index <= minute_passed :
                                if float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < 1.003 and v_c/v_o < 1.003 and float(candles1m[candles1m_highest_index][2])/float(candles1m[candles1m_highest_index][3]) < v_h/v_l:
                                    candles1m_second_highest_index = candles1m_highest_index
                                    candles1m_highest_index = index
                                elif float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < 1.003 and v_c/v_o > 1.003  and float(candles1m[candles1m_highest_index][2])/float(candles1m[candles1m_highest_index][3]) < v_change:
                                    candles1m_second_highest_index = candles1m_highest_index
                                    candles1m_highest_index = index
                                elif float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) > 1.003 and v_c/v_o < 1.003 and float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < v_h/v_l:
                                    candles1m_second_highest_index = candles1m_highest_index
                                    candles1m_highest_index = index
                                elif float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) > 1.003 and v_c/v_o > 1.003 and float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < v_change:
                                    candles1m_second_highest_index = candles1m_highest_index
                                    candles1m_highest_index = index

                        for index,candle in enumerate(candles1m):
                            v_o = float(candle[1])
                            v_h = float(candle[2])
                            v_l = float(candle[3])
                            v_c = float(candle[4])
                            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
                            v_change = abs(v_c/v_o - 0.9999)
                            if index < minute_passed and index != candles1m_highest_index:
                                avg_amp = avg_amp + v_change
                                avg_amp_count = avg_amp_count + 1
                        if avg_amp_count != 0:
                            avg_amp = avg_amp/avg_amp_count
                        writeLog(symbol + " minute_passed " + str(minute_passed) + " red_candle_count " + str(red_candle_count) + " candles1m_highest_index " + str(candles1m_highest_index) + " candles1m_second_highest_index " + str(candles1m_second_highest_index) + " below_v_c_count " + str(below_v_c_count) + " avg_amp " + str(avg_amp))
                        if tradeType == "TRADE_INFO_K" and red_candle_count >= 4:
                            marketSell("REACH_MAX_F",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif float(candles1m[0][2])/float(candles1m[0][3]) - 1 > 0 and abs(float(candles1m[0][4])/float(candles1m[0][1]) - 1)/(float(candles1m[0][2])/float(candles1m[0][3]) - 1) < 0.6 and float(candles1m[0][2])/float(candles1m[0][3]) > 1.035:
                            marketSell("REACH_MAX_F_1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif ((float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < 1.002 and candles1m_highest_index != 0) or (float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < 0.997 and candles1m_highest_index == 0)) and float(candles1m[candles1m_highest_index][2])/float(candles1m[candles1m_highest_index][3]) > 1.0125:
                            writeLog("REACH_MAX_F_2 " + str(candles1m_highest_index))
                            marketSell("REACH_MAX_F_2",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif ((float(candles1m[candles1m_second_highest_index][4])/float(candles1m[candles1m_second_highest_index][1]) < 1.002 and candles1m_second_highest_index != 0) or (float(candles1m[candles1m_second_highest_index][4])/float(candles1m[candles1m_second_highest_index][1]) < 0.997 and candles1m_second_highest_index == 0)) and float(candles1m[candles1m_second_highest_index][2])/float(candles1m[candles1m_second_highest_index][3]) > 1.0125:
                            writeLog("REACH_MAX_F_3 " + str(candles1m_second_highest_index))
                            marketSell("REACH_MAX_F_3",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif minute_passed >= 4 and below_v_c_count/minute_passed > 0.75:
                            marketSell("REACH_MAX_F_4",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif candles1m_highest_index != 0 and avg_amp != 0 and float(candles1m[candles1m_highest_index][4])/float(candles1m[candles1m_highest_index][1]) < 1.002 and float(candles1m[candles1m_highest_index][2])/float(candles1m[candles1m_highest_index][3]) > 1.0075 and (float(candles1m[candles1m_highest_index][2])/float(candles1m[candles1m_highest_index][3])-1)/avg_amp > 2 and avg_amp > 0.005:
                            marketSell("REACH_MAX_F_5",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif buyPrice/lowestPrice > 1.0175:
                            marketSell("REACH_MAX_H",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif time.time() - buyPriceTime > 300 and firstBidPrice/buyPrice < 1.007 and firstBidPrice/buyPrice > 1.003:
                            marketSell("REACH_MAX_G",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                elif time.time() - buyPriceTime >= min_hold_duration :
                    if firstBidPrice / buyPrice <= immediate_lower_threshhold_to_sell:
                        marketSell("BUSTED",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif (highestPrice - buyPrice) !=0 and (firstBidPrice- buyPrice) / (highestPrice - buyPrice) <= 0.85 and firstBidPrice / buyPrice <= price_falling_threshold_to_sell and firstBidPrice / buyPrice >= floor_threshold_to_sell:  
                        marketSell("OUT_HYPE_ZONE",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    elif time.time() - verifyTime > 60*17:
                        if verifyPrice ==0 or firstBidPrice/verifyPrice > 1.005 :
                            buyPairs[symbol] = {"price" : buyPrice, "time": buyPriceTime, "verify_time" : time.time(), "verify_price" : firstBidPrice, "trade_type" : tradeType, "change" : change,"buy_sel_ratio": buy_sel_ratio}
                        else:
                            marketSell("PRICE_NOT_GOING_UP",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                    else:
                        candles1m = client.get_historical_klines(symbol, Client.KLINE_INTERVAL_1MINUTE, "15 minutes ago")
                        #candles1m = candles1m[:-1]
                        candles1m = list(reversed(candles1m))
                        low_vol_candle = 0 
                        minute_passed = round((time.time() - buyPriceTime)/60)
                        highest_green_vol = 1
                        for index,candle in enumerate(candles1m[:minute_passed]):
                            v_o = float(candle[1])
                            v_h = float(candle[2])
                            v_l = float(candle[3])
                            v_c = float(candle[4])
                            v_vol = float(candle[7]) if float(candle[7]) > 0 else 1
                            if v_vol < 1000:
                                low_vol_candle = low_vol_candle + 1
                            if v_vol > highest_green_vol :
                                highest_green_vol = v_vol
                        # if float(candles1m[0][1])/float(candles1m[0][4]) >= 1.003 and float(candles1m[1][1])/float(candles1m[1][4]) > 1.001 and float(candles1m[2][1])/float(candles1m[2][4]) > 1.001 and float(candles1m[0][7]) > 10000 and float(candles1m[1][7]) > 10000:
                        #     marketSell("HAS_HIGH_VOL_RED_CANDLES",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif low_vol_candle >= 7 and firstBidPrice / buyPrice > 0.996:
                        #     marketSell("HAS_LOW_VOL_CANDLES",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # if float(candles1m[0][1])/float(candles1m[0][4]) > 1.003 and float(candles1m[1][1])/float(candles1m[1][4]) > 1.003 and float(candles1m[2][1])/float(candles1m[2][4]) > 1.003:
                        #     marketSell("REACH_MAX_A",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif float(candles1m[1][4])/float(candles1m[1][1]) > 1.003 and float(candles1m[2][1])/float(candles1m[2][4]) > 1.003 and float(candles1m[3][1])/float(candles1m[3][4]) > 1.003:
                        #     marketSell("REACH_MAX_B",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        if firstBidPrice / buyPrice < 1.1 and firstBidPrice / buyPrice >= price_falling_threshold_to_sell and highestPrice/buyPrice - firstBidPrice/buyPrice > 0.015:  
                            marketSell("REACH_MAX_C",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif firstBidPrice / buyPrice < 1.1 and firstBidPrice / buyPrice <= 0.985 and highestPrice/buyPrice - firstBidPrice/buyPrice > 0.02:  
                        #     marketSell("REACH_MAX_D",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif firstBidPrice / buyPrice < 1.1 and firstBidPrice / buyPrice <= 0.985 and highestPrice/buyPrice - firstBidPrice/buyPrice > 0.01 and float(candles1m[0][7])/highest_green_vol > 1.1 and float(candles1m[0][1])/float(candles1m[0][4]) > 1.005: 
                        #     marketSell("REACH_MAX_D1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif firstBidPrice / buyPrice < 1.002 and time.time() - buyPriceTime > 600:
                        #     marketSell("REACH_MAX_E",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif tradeType == "TRADE_INFO_A" and time.time() - buyPriceTime > 60*5 and firstBidPrice/buyPrice > 1.003 and firstBidPrice/buyPrice < 1.03:
                            marketSell("REACH_MAX_F",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif tradeType == "TRADE_INFO_A" and (highestPrice - buyPrice) !=0 and (firstBidPrice- buyPrice) / (highestPrice - buyPrice) <= 0.85 and firstBidPrice/buyPrice > 1.01 and firstBidPrice/buyPrice < 1.03:
                            marketSell("REACH_MAX_F1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif tradeType == "TRADE_INFO_A" and firstBidPrice/buyPrice > 1.05:
                            marketSell("REACH_MAX_F2",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif time.time() - buyPriceTime > 45 and time.time() - buyPriceTime < 90 and datetime.now().second > 30 and float(candles1m[0][7])/float(candles1m[1][7]) < 0.2:
                        #     marketSell("REACH_MAX_F1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif buy_sel_ratio > buy_vol/sell_vol and buy_sel_ratio < 3 and buy_sel_ratio*0.66 > (buy_vol/sell_vol):
                        #     marketSell("REACH_MAX_H",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        # elif buy_sel_ratio > buy_vol/sell_vol and buy_sel_ratio > 3 and buy_sel_ratio/3 > (buy_vol/sell_vol):
                        #     marketSell("REACH_MAX_H1",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
                        elif firstBidPrice/buyPrice > 1.002 and buyPrice/lowestPrice > 1.02 and firstBidPrice/buyPrice < 1.01 and time.time() - buyPriceTime >= 60*30:
                            marketSell("REACH_MAX_I",symbol,buyPrice,buyPriceTime,firstBidPrice,highestPrice,lowestPrice,asset_quantity)
            else:
                writeLog("CANNOT_SELL " + symbol + " not enough usdt on market " + str(firstBidPrice * firstBidQuantity))

    if totalHoldingProfit != 0:
        writeLog("TOTAL_HOLDING_PROFIT: " + str(totalHoldingProfit) +" TOTAL PROFIT: " + str(totalProfit + totalHoldingProfit))

                

writeLog(" ------- BEGIN -------")
getBallances()

while True:
    try:
        writeLog("-----------------------------------------------------")
        #loopCheck(isBTCPumpingDown())
        loopCheck(False)
        if len(buyPairs) > 0:
            time.sleep(delay_per_turn_holding) 
        else:
            time.sleep(delay_per_turn)
    except Exception as exception:
        #writeLog(symbol +" EXCEPTION " + str(exception))
        print(datetime.now().strftime("%d/%m/%Y %H:%M:%S") +" " + str(traceback.format_exc()))
        
